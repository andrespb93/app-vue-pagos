import Vue from "vue";
import Router from "vue-router";

// Containers
// const DefaultContainer = () => import("@/containers/DefaultContainer");

// Views
const Pay = () => import("@/views/pay/pay");
const PayDebtorInvoice = () => import("@/views/pay/PayDebtorInvoice");


const Page404 = () => import("@/views/pages/Page404");
const Page500 = () => import("@/views/pages/Page500");

Vue.use(Router);

export default new Router({
  mode: "hash", // https://router.vuejs.org/api/#mode
  linkActiveClass: "open active",
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    /* {
      path: "/",
      redirect: "dashboard",
      name: "Inicio",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard
        },
        // -------------------------------------------------
        // URL´S PARA EL USUARIO
        // -------------------------------------------------
        // {
        //   path: "users",
        //   meta: { label: "Usuarios" },
        //   component: {
        //     render(c) {
        //       return c("router-view");
        //     }
        //   },
        //   children: [
        //     {
        //       path: "",
        //       component: User
        //     }
        //   ]
        // },

      ]
    }, */
    {
      path: "",
      name: "Pages",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      children: [
        {
          // Vista para cambiar la contraseña sin loguearse
          path: "/pay",
          name: "pay",
          component: Pay
        },
        {
          path: "/pay-debtor-invoce/:debtor",
          name: "pay-debtor-invoce",
          component: PayDebtorInvoice
        },
        {
          path: "404",
          name: "Page404",
          component: Page404
        },
        {
          path: "500",
          name: "Page500",
          component: Page500
        }
      ]
    }
  ]
});
