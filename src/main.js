// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "core-js/es6/promise";
import "core-js/es6/string";
import "core-js/es7/array";
// import cssVars from 'css-vars-ponyfill'
import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import App from "./App";
import router from "./router";
import VueResource from "vue-resource";
import VueSweetalert2 from "vue-sweetalert2";
import Multiselect from "vue-multiselect";
import JsonExcel from "vue-json-excel";
import datePicker from "vue-bootstrap-datetimepicker"; // https://www.npmjs.com/package/vue-bootstrap-datetimepicker
import VueMask from "v-mask";
import VoerroTagsInput from "@voerro/vue-tagsinput"; // https://github.com/voerro/vue-tagsinput
import FullCalendar from "vue-full-calendar"; // https://www.npmjs.com/package/vue-full-calendar
// import vSelect from 'vue-select'// https://www.npmjs.com/package/vue-select
import VueFormWizard from "vue-form-wizard"; //https://binarcode.github.io/vue-form-wizard/?ref=madewithvuejs.com#/
import { VueReCaptcha } from "vue-recaptcha-v3";

// import VueGoodTablePlugin from 'vue-good-table';

//PLANTILLA: https://coreui.io/docs/2.1/components/tooltips/

const options = {
  confirmButtonColor: "#41b882",
  cancelButtonColor: "#ff7674"
};

Vue.prototype.$reload = { value: true };
Vue.prototype.$urlApi = "http://127.0.0.1:8000/api/";
// Vue.prototype.$urlApi = "https://devapi.treboljuridico.com/api/";
Vue.prototype.$maxMinutes = 10000000000000; // 570

//import 'vue-select/dist/vue-select.css';

// todo
Vue.use(VueResource);
Vue.use(BootstrapVue);
Vue.use(datePicker);
Vue.use(VueMask);
Vue.use(VueSweetalert2, options);
Vue.use(VueFormWizard);
// Vue.use(VueGoodTablePlugin);
Vue.component("multiselect", Multiselect);
Vue.component("downloadExcel", JsonExcel);
Vue.component("tags-input", VoerroTagsInput);
Vue.component("pagination", require("laravel-vue-pagination")); // https://github.com/gilbitron/laravel-vue-pagination
Vue.use(VueReCaptcha, {
  siteKey: "6LcHmKQZAAAAAK7oqkCG1iHi2BPxAzl4c65GePkH",
  loaderOptions: {
    useRecaptchaNet: true
  }
});

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  template: "<App/>",
  components: {
    App
  }
});
