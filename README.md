# Frontend - App

<p align="center"><img src="https://vuejs.org/images/logo.png" width="64"></p>

## Instalación

``` bash
# install app's dependencies
$ npm install

```

## Uso
``` bash
# serve with hot reload at localhost:8080
$ npm run serve

# build for production with minification
$ npm run build

# run linter
$ npm run lint

# run unit tests
$ npm run test:unit

# run e2e tests
$ npm run test:e2e

```