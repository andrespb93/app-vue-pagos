//$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

$(document).ready(function () {
  // ESTA PARTE DE CODIGO SE HACE PARA CORREGIR EL BUG DE <<Bootstrap 4 >>, EN LAS TABS
  $(document).on("click", ".nav-item a", function (e) {
    e.preventDefault();
    $(".nav-item a").removeClass("active");
  });
});

function showLoader() {
  $("body.app-content").block({
    message: '<h1><i class="fa fa-circle-o-notch fa-spin"></i><h6>Cargando...</h6></h1>',
    overlayCSS: {
      background: "rgba(24, 44, 68, 0.8)",
      opacity: 1,
      cursor: "wait",
      "z-index": 99998
    },
    css: {
      width: "10%",
      border: "none",
      backgroundColor: "transparent",
      color: "#fff",
      "z-index": 99999
    }
  });
}

function hideLoader() {
  $("body.app-content").unblock();
}

function validatePassword(password) {
  if (password.length >= 8) {
    var mayuscula = false;
    var minuscula = false;
    var numero = false;
    var caracter_especial = false;

    for (var i = 0; i < password.length; i++) {
      if (password.charCodeAt(i) >= 65 && password.charCodeAt(i) <= 90) {
        mayuscula = true;
      } else if (password.charCodeAt(i) >= 97 && password.charCodeAt(i) <= 122) {
        minuscula = true;
      } else if (password.charCodeAt(i) >= 48 && password.charCodeAt(i) <= 57) {
        numero = true;
      } else {
        caracter_especial = true;
      }
    }
    if (mayuscula == true && minuscula == true && caracter_especial == true && numero == true) {
      return true;
    }
  }
  return false;
}

function validEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

/**
 * @method calcularDigitoVerificacion
 * Para validar el digito de veficicación
 *
 * @param {*} myNit
 */
function calcularDigitoVerificacion(myNit) {
  var vpri, x, y, z;

  // Se limpia el Nit
  myNit = myNit.replace(/\s/g, ""); // Espacios
  myNit = myNit.replace(/,/g, ""); // Comas
  myNit = myNit.replace(/\./g, ""); // Puntos
  myNit = myNit.replace(/-/g, ""); // Guiones

  // Se valida el nit
  if (isNaN(myNit)) {
    console.log("El nit/cédula '" + myNit + "' no es válido(a).");
    return "";
  }

  // Procedimiento
  vpri = new Array(16);
  z = myNit.length;

  vpri[1] = 3;
  vpri[2] = 7;
  vpri[3] = 13;
  vpri[4] = 17;
  vpri[5] = 19;
  vpri[6] = 23;
  vpri[7] = 29;
  vpri[8] = 37;
  vpri[9] = 41;
  vpri[10] = 43;
  vpri[11] = 47;
  vpri[12] = 53;
  vpri[13] = 59;
  vpri[14] = 67;
  vpri[15] = 71;

  x = 0;
  y = 0;
  for (var i = 0; i < z; i++) {
    y = myNit.substr(i, 1);
    // console.log ( y + "x" + vpri[z-i] + ":" ) ;

    x += y * vpri[z - i];
    // console.log ( x ) ;
  }

  y = x % 11;
  // console.log ( y ) ;

  return y > 1 ? 11 - y : y;
}

function setPaginator(data) {
  let pagination = {
    current_page: data.current_page,
    path: data.path,
    last_page: data.last_page,
    first_page_url: data.first_page_url,
    next_page_url: data.next_page_url,
    prev_page_url: data.prev_page_url,
    last_page_url: data.last_page_url
  };

  return pagination;
}

function darNumeroPaginaPaginacion(urlPage) {
  let page = 1;

  if (urlPage) {
    // SI SE ENVIA UNA URL SE BUSCA EL NUMERO DE LA PAGINA
    let index = urlPage.indexOf("=") + 1;
    page = urlPage.substr(index);
  }

  return page;
}

function getBadge(status) {
  return status === "A" ? "success" : status === "I" ? "danger" : status === "P" ? "primary" : "";
}

function getTextStatus(status) {
  return status === "A" ? "Activo" : status === "I" ? "Inactivo" : status === "P" ? "Por activar" : "";
}

function date24To12(time) {
  // Verificar el formato de hora correcto y dividir en componentes
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) {
    // Si el formato de hora es correcto
    time = time.slice(1); // Eliminar el valor de coincidencia de cadena completa
    time[5] = +time[0] < 12 ? "AM" : "PM"; // Colocarl AM/PM
    time[0] = +time[0] % 12 || 12; // Ajustar horas
  }

  return time.join("");
}

function getMsgError(error) {
  let errores = [];

  if (Array.isArray(error.body.message)) {
    let data = Object.values(error.body.message);
    errores = data;
  } else {
    errores.push(error.body.message);
  }

  return errores;
}

function sumarDias(fecha, dias) {
  fecha.setDate(fecha.getDate() + dias);
  return fecha;
}

function JQueryMask(value, mask) {
  var tempInput = $(document.createElement("input"));
  tempInput.val(value);
  tempInput.mask(mask /*'(+00) 000 000'*/, { reverse: false /* true */ });
  var mascara = tempInput.val();
  return mascara;
}

function formatPrice(value) {
  let val = (value / 1).toFixed(2).replace(".", ",");
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function roundToTwo(num) {
  return +(Math.round(num + "e+2") + "e-2");
}

function formatPrice(value) {
  let val = Math.trunc(value);
  // let val = (value / 1).toFixed(2).replace(".", ",");
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function getNameMonth(number_month) {
  switch (number_month) {
    case "01":
      return "Enero";
    case "02":
      return "Febrero";
    case "03":
      return "Marzo";
    case "04":
      return "Abril";
    case "05":
      return "Mayo";
    case "06":
      return "Junio";
    case "07":
      return "Julio";
    case "08":
      return "Agosto";
    case "09":
      return "Septiembre";
    case "10":
      return "Octubre";
    case "11":
      return "Noviembre";
    case "12":
      return "Diciembre";
    default:
      return "--";
  }
}

function GformatPrice(value) {
  let val = Math.trunc(value);
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}